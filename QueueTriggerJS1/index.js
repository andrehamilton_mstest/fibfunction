module.exports = function (context, myQueueItem) {
    context.log("git integration enabled");
    context.log('JavaScript queue trigger function processed work item', myQueueItem);
    context.done();
};