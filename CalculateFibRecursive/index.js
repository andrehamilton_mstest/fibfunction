
module.getFib = function(n) {
    if(n <= 1){
        return 1;
    }

    return module.getFib(n-1) + module.getFib(n-2);
};

module.exports = function (context, input) {
    var result = module.getFib(20);
    context.log("git integration enabled");
    context.log('JavaScript manually triggered function called with input:', input);
    context.log("Fib of 20 is " + result);
    context.done();
};